import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

//daqui para baixo são importações dos meus componentes
import LandingPageFunction from './components/landingPage/index'
import Login from './pages/loginPage/index'
import HomeCliente from './pages/homeCliente/index'
import HomeProfissional from './pages/homeProfissional/index'
import HomeAdmin from './pages/homeAdmin/index'
import SignIn from './pages/signInPage/index'
import Exemplo from './pages/exemplo/index'
import Upload from './pages/cadastroCategoria/index'
import CardsExample from './pages/cardsExample/index'
import CounterComponent from './components/useeffectTeste/index'
//aqui é a função que define qual rota está associada com cada padrão de URL
function Routes() {
    return (
        <Switch>
            <Route exact path='/' component={LandingPageFunction} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/cliente' component={HomeCliente} />
            <Route exact path='/profissional' component={HomeProfissional} />
            <Route exact path='/admin' component={HomeAdmin} />
            <Route exact path='/cadastro' component={SignIn} />
            <Route exact path="/exemplo" component={Exemplo} />
            <Route exact path="/servico" component={Upload} />
            <Route exact path="/counter" component={CounterComponent} />
            <Route exact path="/cards" component={CardsExample} />
            <Redirect from='*' to='/' />
        </Switch>
    )
}

export default Routes;