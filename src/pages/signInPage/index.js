import React, { useState } from 'react';
import Logo from '../../assets/126.png'
import Background from '../../assets/background.jpg'
import passwordValidator from 'password-validator';
import { useHistory } from 'react-router-dom'
import { Button, Form } from 'react-bootstrap';
import jwt from 'jsonwebtoken'

function SignIn() {

    const [login, setLogin] = useState("");
    const [senha, setSenha] = useState("");
    const [nome, setNome] = useState("");
    const [tipo, setTipo] = useState("Cliente");
    const [cpf, setCPF] = useState("");
    const [instrucaoSenha, setInstrucaoSenha] = useState("");
    const history = useHistory();

    const routeChange = (name) => {
        let path = `/`.concat(name);
        history.push(path);//joga o usuário pra essa path
    }

    function changeType(clicado){
        setTipo(clicado)
    }

    async function sendInput() {
        try {//tente executar as linhas de 25 até 37
            let retorno = await fetch('http://localhost:5000/users/cadastro', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify({login: login, senha : senha, nome: nome, tipo: tipo, cpf:cpf})
            });
            
            let json = await retorno.json()
            console.log(json);
            return retorno;
        } catch (error) { //em caso de erro, faça um print do erro
            console.error(error);
        }
    }

    return (
        <div>
            <h1 className={"textoTopo"}> <p>O melhor dos serviços </p><p>com a maior agilidade!</p></h1>
            <a href={'/'}>
                <img className="Logo" style={{ height: "200px", width: "200px" }} src={Logo} alt={'Logo do Website'} />
            </a>
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Control type="email" placeholder="Email" value={login} onChange={(text) => setLogin(text.target.value)} />
                    <Form.Text className="text-muted">
                        Seu email nunca será compartilhado por nós.
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Control type="password" placeholder="Senha" value={senha} onChange={(text) => setSenha(text.target.value)} />
                </Form.Group>
                <Form.Group controlId="formBasicName">
                    <Form.Control placeholder="Nome" value={nome} onChange={(text) => setNome(text.target.value)} />
                </Form.Group>
                <Form.Group controlId="formGridState">
                <Form.Label>State</Form.Label>
                <Form.Control as="select" defaultValue={tipo} onChange={(evt)=> changeType(evt.target.value)}>
                    <option>Cliente</option>
                    <option>Profissional</option>
                </Form.Control>
                </Form.Group>
                <Form.Group controlId="formBasicCPF">
                    <Form.Control  placeholder="Cpf" value={cpf} onChange={(text) => setCPF(text.target.value)} />
            
                </Form.Group>
            </Form>
            <Form.Text className="text-muted">
                {instrucaoSenha}
            </Form.Text>
            
            <Button className={"btn-custom"}  onClick={() => sendInput()}  > Cadastrar </Button>
        </div>
    )
}

export default SignIn;