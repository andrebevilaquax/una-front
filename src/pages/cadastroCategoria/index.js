import React, { useState } from 'react';
import ImageUploader from 'react-images-upload';

function Upload() {
    const formData = new FormData();
    const [pictures, setPictures] = useState();
    // const [descricao, setDescricao] = useState();
    // const [categoria, setCategoria] = useState();
    //image, descricao, categoria
    async function cadastroCategoria() {
        console.log("imagem com todos os dados", pictures)
        console.log("imagem posicao 0", pictures[0])
        formData.append('image', pictures[0]);
        formData.append('descricao', "descricao");
        formData.append('categoria', "categoria");   
        formData.append('token', localStorage.getItem('token')); 
        console.log(localStorage.getItem('token'))  
        try {
            let options = {
                method: 'POST',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                body: formData
            }
            console.log(formData)
            delete options.headers['Content-Type'];

            let retorno = await fetch('http://localhost:5000/services', options);
        } catch (error) { //em caso de erro, faça um print do erro
            console.error(error);
        }
    }
    const onDrop = picture => {
        setPictures(picture);
    };
    return (
        <div>
            <ImageUploader
                withIcon={true}
                buttonText='Choose images'
                onChange={onDrop}
                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                maxFileSize={5242880}
            />
            
            <button onClick={cadastroCategoria}> Cadastrar Categoria </button>
        </div>
    );

}


export default Upload;