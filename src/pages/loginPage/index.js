import React, { useState } from 'react';
import Logo from '../../assets/126.png'
import Background from '../../assets/background.jpg'
import './Login.css'
import passwordValidator from 'password-validator';
import { useHistory } from 'react-router-dom'
import { Button, Form, NavLink } from 'react-bootstrap';
import jwt from 'jsonwebtoken'

function Login() {

    const [login, setLogin] = useState("");
    const [senha, setSenha] = useState("");
    const [token, setToken] = useState("");
    const [instrucaoSenha, setInstrucaoSenha] = useState("");
    const history = useHistory();

    const routeChange = (name) => {
        let path = `/`.concat(name); 
        history.push(path);//joga o usuário pra essa path
    }

    async function testBackEnd() {
        
        try {//tente executar as linhas de 25 até 37

            let retorno = await fetch('http://localhost:5000/users/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify({login: login, senha : senha})
            });
            
            // retorno = { token:23984q78254,   tipo:admin}


            let json = await retorno.json();


            localStorage.setItem('token', json.token);

            console.log(localStorage.getItem("token"))
            
            if(json.tipo){//se voltou algum dado de tipo, quer dizer que o usuário está autorizado
                //funcao de desencriptação tipo
                routeChange(json.tipo);
            }
            else{
                alert("Usuário não autorizado")
            }
            


        } catch (error) { //em caso de erro, faça um print do erro
            console.error(error);
        }
    }

    return (
        <div>
            <div className={"Login"}>

                <div className={"divInterna"} >

                    <h1 className={"textoTopo"}> <p>O melhor dos serviços </p><p>com a maior agilidade!</p></h1>
                    <a href={'/'}>
                        <img className="Logo" style={{ height: "200px", width: "200px" }} src={Logo} alt={'Logo do Website'} />
                    </a>
                    <Form>
                        <Form.Group controlId="formBasicEmail">  
                            <Form.Control type="email" placeholder="Email" value={login.toString()} onChange={(text) => setLogin(text.target.value)} />
                            <Form.Text className="text-muted">
                                Seu email nunca será compartilhado por nós.
                             </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Control type="password" placeholder="Senha" value={senha.toString()} onChange={(text) => setSenha(text.target.value)} />

                        </Form.Group>

                    </Form>
                    <Form.Text className="text-muted">
                        {instrucaoSenha}
                    </Form.Text>
                    <Button className={"btn-custom"}
                        onClick={() => {
                            testBackEnd();
                        }
                        }
                    >
                        Login
                    </Button>
                    <NavLink onClick={evt => routeChange("cadastro")} > Cadastrar-se!</NavLink>
                </div>

            </div>
        </div>
    )
}


// function validarSenha(senha){
//     var schema = new passwordValidator();
//     schema
//     .is().min(8)                                    // Minimum length 8
//     .is().max(100)                                  // Maximum length 100
//     .has().uppercase()                              // Must have uppercase letters
//     .has().lowercase()                              // Must have lowercase letters
//     .has().digits(2)                                // Must have at least 2 digits
//     .has().not().spaces()                           // Should not have spaces
//     .is().not().oneOf(['Passw0rd', 'Password123']); // Blacklist these values
//     return schema.validate(senha)
// }
// if(validarSenha({senha}) == false){
//     setInstrucaoSenha("Sua senha deve conter entre 8 e 100 dígitos, 1 letra maiúscula, 1 letra minúscula, dois dígitos e não deve conter espaços")
//     setSenha("")
// }
// else{}

function checaCredenciais(login, senha) {
    console.log("Login ", login, " senha ", senha)
    if (login == jsonLoginCorreto.login && senha == jsonLoginCorreto.senha) {
        var privateKey = "minhaChaveSuperSecreta";
        var token = jwt.sign({ foo: 'bar' }, privateKey);
        return [token, jsonLoginCorreto.tipoPerfil];
    }
    else {
        alert("Login ou senha incorretos");
        return [];
    }

}
let jsonLoginCorreto = {
    "login": "andre",
    "senha": "12345",
    "tipoPerfil": "cliente"
}



export default Login;




// em linhas ou colunas, usando flex
//  ou em linhas e colunas, usando grid