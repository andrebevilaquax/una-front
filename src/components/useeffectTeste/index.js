import React, { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';

function CounterComponent() {

    const [contador, setContador] = useState(0);
                                   //vetor    objeto[0]     objeto[1]
    const [array, setArray] = useState(   [    ]    );
    
    // função - efeito que vai ser desencadeado
    // array - indica os conteúdos que vão ser monitorados para disparar a função
    useEffect(
        () => {
            fetchArrayData();
            document.title = contador ;
        }, //efeito
        [contador] //gatilho
    );

    async function fetchArrayData(){
        try {
            let retorno = await fetch('http://localhost:5000/users/array', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
            });
           
            let json = await retorno.json();
            

            setArray([...array,...json])
    
            //console.log(json);

        } catch (error) { //em caso de erro, faça um print do erro
            console.error(error);
        }
    }

    
    return (
        <div>

            <h1>O valor do contador 1 é {contador} </h1>
            <button onClick={() => setContador(contador + 1)}> Incrementar </button>



            {//indicando que o componente que vai ser renderizado tem mais de 1 estado
                 array.length == 0 ?                   
                    <h1>Os dados ainda não estão prontos </h1> :

                    array.map(({ id }) =>
                        <h1>O id desse dado é {id} </h1>
                    )
            }
            {/* <button onClick={() => fetchArrayData() }> Trazer os dados </button> */}


        </div>
    )
}
//setArray([...array,  { "id": 1 }  , { "id":2  } ]
export default CounterComponent;


{/* <Card style={{ width: '18rem' }}>
                        <Card.Body>
                            <Card.Title>Card Title</Card.Title>
                            <Card.Text>
                                Texto do card
                        </Card.Text>
                            <Button variant="primary">Redirecionar</Button>
                        </Card.Body>
                    </Card> */}