
import React, { useState } from "react";
import './Landing.css'


const LandingPageFunction = props => {
//   const [state, setState] = useState({"algumatag":"algumvalor"});

        return(
            <div>
                
                <div className="dark-header grid">
                    <div className="conteudo">
                        <div className="info-marca row">
                            <img className="logo" alt="logo_site" src={require('../../assets/126.png')}></img>;               
                            <h2 className="nome_marca"> ServGenda </h2>
                        </div>
                        <h1 className="titulo">Procure ou ofereça serviços em um só lugar!</h1>
                        <h3 className="subtitulo">Conecte-se ao melhor hub de profissionais autônomos do Brasil!</h3>
                    </div>
                </div>
                
                <div className="dark-header-divider"></div>


                <div className="light-header grid">
                    <div clasclassNames="conteudo">
                        <div className="lead_email" action="/email" method="POST">
                            <input type="email" name="email" placeholder="Digite seu email e cadastre-se!"/>
                            <button type="submit">Inscreva-se!</button>
                        </div>
                    </div>
                </div>

                <div className="light-header-divider"></div>

           </div>
        )
}


export default LandingPageFunction;